module schema-translator

go 1.16

require (
	github.com/mattn/go-colorable v0.1.8
	github.com/sirupsen/logrus v1.8.1
)
