test

```mermaid

erDiagram
  CAR ||--o{ NAMED-DRIVER : allows

  %% id : primary
  CAR {
    string registrationNumber
    string make
    string model
  }
  PERSON ||--|| NAMED-DRIVER : is
  PERSON {
    string firstName
    string lastName
    int age
  }


```
