package constants

// AppName : このアプリケーションの名前
const AppName = "SchemaTranslator"

// LowerAppName : 小文字にしたこのアプリケーションの名前
const LowerAppName = "schema-translator"

// Version : このアプリケーションのバージョン
const Version = "v0.0.1"
