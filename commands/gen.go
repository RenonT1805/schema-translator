package commands

import (
	"flag"
	"fmt"
	"schema-translator/constants"

	log "github.com/sirupsen/logrus"
)

// Gen : gen コマンド
type Gen struct {
	FlagSet    *flag.FlagSet
	SourceFile string
	OutFile    string
	Target     string
}

// コマンドの使い方
func (g Gen) showCommandUsage() {
	out := log.StandardLogger().Out
	flag.CommandLine.SetOutput(out)
	fmt.Fprintf(out, "\n使い方: %s %s [options...]\n", constants.LowerAppName, g.CommandName())
	fmt.Fprintf(out, "\n説明: %s\n", g.CommandDescription())
	fmt.Fprintf(out, "\n実行オプション:\n")
	g.FlagSet.PrintDefaults()
}

// GetCommandName : コマンドの名前
func (g Gen) CommandName() string {
	return "gen"
}

// GetCommandDescription : コマンドの説明文
func (g Gen) CommandDescription() string {
	return "mermaidのERDiagramからSQLのスキーマを生成します。"
}

// initialize : 初期化
func (g *Gen) initialize(args []string) {
	g.FlagSet = flag.NewFlagSet(g.CommandName(), flag.ExitOnError)
	g.FlagSet.Usage = g.showCommandUsage
	g.FlagSet.StringVar(&g.SourceFile, "src", "", "mermaid source file")
	g.FlagSet.StringVar(&g.OutFile, "out", "", "output file")
	g.FlagSet.StringVar(&g.Target, "format", "", "target format")
	g.FlagSet.Parse(args)
}

// Run : gen command entry point
func (g Gen) Run(args []string) constants.ResponseCode {
	return constants.Successful
}

func generateSchema(source string) string {
	return ""
}
