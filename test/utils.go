package test

type DummyWriter struct {
}

func (d DummyWriter) Write(p []byte) (n int, err error) {
	return len(p), err
}
