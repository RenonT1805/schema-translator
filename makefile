build-parser:
	cd parser && goyacc -o mermaid_parser.go mermaid_parser.go.y

build:
	make build-parser
	go build