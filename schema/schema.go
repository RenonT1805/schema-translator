package schema

type ColumnAttribute int

const (
	Primary ColumnAttribute = iota + 1
	Unique
)

type Table struct {
	name    string
	colmuns []Colmun
}

type Colmun struct {
	name       string
	attributes []ColumnAttribute
}
