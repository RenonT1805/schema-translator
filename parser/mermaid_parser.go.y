%{
package parser

import (
    "text/scanner"
    "strings"
)

type ColumnType int
type StatementType int
type DiagramType int
type Relationship int
type Expression interface{}

const (
    One Relationship = iota + 1
    ZeroOrMore
    OneOrMore
)

const (
    Int ColumnType = iota + 1
    String
)

const (
    DefineTableStatement          StatementType = iota + 1
    RelationalExpressionStatement
)

const (
    ERDiagram DiagramType = iota + 1
)

type Diagram struct {
    Type       DiagramType
    Statements []Statement
}

type Statement struct {
    Type StatementType
    Expr Expression
}

type Table struct {
    Name       string
    Columns    []Column
    Attributes []Attribute
}

type Attribute struct {
    Name string
    Params []string
}

type Column struct {
    Name string
    Type ColumnType
}

type RelationalItem struct {
    Table        string
    Relationship Relationship
}

type RelationalExpression struct {
    Left  RelationalItem
    Right RelationalItem
    Label string
}

type Token struct {
    token   int
    literal string
}

%}

%union{
    token                      Token
    statement                  Statement
    statements                 []Statement
    diagram                    Diagram
    diagrams                   []Diagram
    column                     Column
    columns                    []Column
    attribute                  Attribute
    attributes                 []Attribute
    param                      string
    params                     []string
    table                      Table
    column_type                ColumnType
    relational_expression      RelationalExpression
    relational_expression_item RelationalItem
}

%type<statements> erstatements
%type<statement> erstatement
%type<diagrams> diagrams
%type<diagram> diagram
%type<columns> columns
%type<column> column
%type<attribute> attribute
%type<attributes> attributes
%type<param> param
%type<params> params
%type<table> table
%type<relational_expression> relational_expression
%type<relational_expression_item> relational_expression_left relational_expression_right

%type<column_type> column_type

%token<token> IDENT ZERO ONE ERDIAGRAM_DECLARATION

%%

diagrams
    :
	{
		$$ = nil
	}
    | diagram diagrams
    {
        if l, isLexer := yylex.(*Lexer); isLexer {
			l.Diagrams = append([]Diagram{$1}, l.Diagrams...)
		}
    }

diagram
    : ERDIAGRAM_DECLARATION erstatements
    {
        $$ = Diagram{Type: ERDiagram, Statements: $2}
    }

erstatements
	:
	{
		$$ = nil
	}
	| erstatement erstatements
	{
		$$ = append([]Statement{$1}, $2...)
	}

erstatement
	: relational_expression
	{
		$$ = Statement{Type: RelationalExpressionStatement, Expr: $1}
	}
    | table
    {
        $$ = Statement{Type: DefineTableStatement, Expr: $1}
    }

table
    : IDENT '{' columns '}'
    {
        $$ = Table{Name: $1.literal, Columns: $3, Attributes: nil}
    }
    | attributes IDENT '{' columns '}'
    {
        $$ = Table{Name: $2.literal, Columns: $4, Attributes: $1}
    }

columns
    :
	{
		$$ = nil
	}
	| column columns
	{
		$$ = append([]Column{$1}, $2...)
	}

column
    : column_type IDENT
    {
        $$ = Column{Type: $1, Name: $2.literal}
    }

attributes
    : attribute
	{
		$$ = []Attribute{$1}
	}
	| attribute attributes
	{
		$$ = append([]Attribute{$1}, $2...)
	}

attribute
    : '%' '%' IDENT ':' params
    {
        $$ = Attribute{Name: $3.literal, Params: $5}
    }

params
    : param
	{
		$$ = []string{$1}
	}
	| param ',' params
	{
		$$ = append([]string{$1}, $3...)
	}

param
    : IDENT
    {
        $$ = $1.literal
    }

column_type
    : IDENT
    {
        var t ColumnType
        switch $1.literal {
        case "int":
            t = Int
        case "string":
            t = String
        }
        $$ = t
    }

relational_expression_left
    : IDENT ONE ONE '-'
    {
        $$ = RelationalItem{Table: $1.literal, Relationship: One}
    }
    | IDENT '}' ZERO '-'
    {
        $$ = RelationalItem{Table: $1.literal, Relationship: OneOrMore}
    }
    | IDENT '}' ONE '-'
    {
        $$ = RelationalItem{Table: $1.literal, Relationship: ZeroOrMore}
    }

relational_expression_right
    : '-' ONE ONE IDENT
    {
        $$ = RelationalItem{Table: $4.literal, Relationship: One}
    }
    | '-' ZERO '{' IDENT
    {
        $$ = RelationalItem{Table: $4.literal, Relationship: ZeroOrMore}
    }
    | '-' ONE '{' IDENT
    {
        $$ = RelationalItem{Table: $4.literal, Relationship: OneOrMore}
    }

relational_expression
    : relational_expression_left relational_expression_right
    {
        $$ = RelationalExpression{Left: $1, Right: $2, Label: ""}
    }
    | relational_expression_left relational_expression_right ':' IDENT
    {
        $$ = RelationalExpression{Left: $1, Right: $2, Label: $4.literal}
    }

%%

type Lexer struct {
    scanner.Scanner
    Diagrams []Diagram
}

func (l *Lexer) Lex(lval *yySymType) int {
    token := int(l.Scan())

    switch token {
    case scanner.EOF:
        return 0
    case scanner.Ident:
        token = IDENT
    }

    switch l.TokenText() {
    case "o":
        token = ZERO
    case "|":
        token = ONE
    case "erDiagram":
        token = ERDIAGRAM_DECLARATION
    }

    lval.token = Token{token: token, literal: l.TokenText()}
    return token
}

func (l *Lexer) Error(e string) {
    panic(e)
}

func GetParseResult(source string) Lexer {
    l := Lexer{}
    l.Init(strings.NewReader(source))
    yyParse(&l)
    return l
}